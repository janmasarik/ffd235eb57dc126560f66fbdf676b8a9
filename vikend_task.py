#!/usr/bin/env python3

import sys
import re
import datetime


# "kernel" of this script
# i use many parameters, so it's memory intensive, but i start this function recursively so i need it
def heart_of_script(data, which, length, string, future_destination, future_arrival_date, future_arrival_time,
                    future_test_string, switch, lst, connection_to_next):
    # divide data
    source = data[which][0]
    destination = data[which][1]
    departure = data[which][2]
    arrival = data[which][3]
    flight_number = data[which][4]
    # you can flight only from place where you come
    if future_destination == source:
        # you need find right time and date
        time_now = regex_time(departure)
        date_now = regex_date(departure)
        # solve date overflow
        return_value = date_solver(date_now, future_arrival_date)
        if return_value == 0:
            if time_solver(time_now, future_arrival_time, False):
                # you need list to determine if you flight to circle
                lst.extend([source, destination])
                if not fly_to_circle(lst):
                    # you board to plane so I need set your next data
                    # and write it to your diary(string)
                    string += source + "," + destination + "," + departure + "," + arrival + "," + flight_number + "\n"
                    connection_to_next += 1
                    future_destination = destination
                    future_arrival_time = regex_time(arrival)
                    future_arrival_date = regex_date(arrival)
        elif return_value == 1:
            if time_solver(time_now, future_arrival_time, True):
                # you board to plane so I need set your next data
                # and write it to your diary(string)
                lst.extend([source, destination])
                if not fly_to_circle(lst):
                    string += source + "," + destination + "," + departure + "," + arrival + "," + flight_number + "\n"
                    connection_to_next += 1
                    future_destination = destination
                    future_arrival_time = regex_time(arrival)
                    future_arrival_date = regex_date(arrival)
    # you go from start of input to the end
    if which != (length-1):
        which += 1
        heart_of_script(data, which, length, string, future_destination, future_arrival_date, future_arrival_time,
                        future_test_string, switch, lst, connection_to_next)
    # but you can forget on something so you need check it again
    # you need switch because when you start with 5 flight, in input list first 4 flight can be acceptable for you
    # and you need check if you find in next iteration next acceptable flight if don't your journey will end
    # and you can write your diary
    else:
        if switch:
            switch = False
            future_test_string = string
            heart_of_script(data, 0, length, string, future_destination, future_arrival_date, future_arrival_time,
                            future_test_string, switch, lst, connection_to_next)
        elif future_test_string != string:
            future_test_string = string
            heart_of_script(data, 0, length, string, future_destination, future_arrival_date, future_arrival_time,
                            future_test_string, switch, lst, connection_to_next)
        else:
            # print(connection_to_next)
            if connection_to_next != 0:
                print(string)


# every time you can flight at least one time, first time
def first_flight(data, length):
    # first flight can be everyone flight from input list
    for i in range(length):
        string = ""
        lst = []
        future_destination = data[i][1]
        future_arrival_time = regex_time(data[i][3])
        future_arrival_date = regex_date(data[i][3])
        string += data[i][0] + "," + data[i][1] + "," + data[i][2] + "," + data[i][3] + "," + data[i][4] + "\n"
        lst.extend([data[i][0], future_destination])
        heart_of_script(data, i, length, string, future_destination, future_arrival_date, future_arrival_time,
                        string, True, lst, 0)


# if date is same you know compare time but if is next day you need check it
def date_solver(date, f_date):
    if date.year == f_date.year and date.month == f_date.month and date.day == f_date.day:
        return 0
    elif (date.day - f_date.day) == 1:
        return 1
    else:
        return 2


# time solver solve if another plane will go between 1 and 4 hours
# problem, plane can go next day if you arrival time is late at night, so day can overflow
# i can solve it with less magic numbers but this was the task
def time_solver(time, f_time, overflow):
    if overflow:
        j = 0
        f_lst_of_hours = [20, 21, 22, 23]
        lst_of_hours = [0, 1, 2, 3]
        for item in range(len(f_lst_of_hours)):
            j += 1
            if f_time.hour == f_lst_of_hours[item]:
                for i in range(j):
                    if lst_of_hours[i] == time.hour:
                        if time.minute > f_time.minute and j == 4:
                            return False
                        elif time.minute < f_time.minute and i == 0:
                            return False
                        else:
                            return True
        return False

    else:
        if 1 < (time.hour - f_time.hour) <= 4:
            if time.minute > f_time.minute and (time.hour - f_time.hour) == 4:
                return False
            elif time.minute < f_time.minute and (time.hour - f_time.hour) == 1:
                return False
            else:
                return True
        else:
            return False


# divide departure and arrival on only date for easier work with date,if format is not right error
def regex_date(data):
    regex_dater = re.search(r'^[0-9]{4}-[0-9]{2}-[0-9]{2}', data, re.M)
    if regex_date:
        date_almost = regex_dater.group()
        date = datetime.datetime.strptime(date_almost, '%Y-%m-%d').date()
        return date
    else:
        return sys.stderr.write("BAD FORMAT OF DATE")


# divide departure and arrival on only time for easier work with time, if format is not right error
def regex_time(data):
    regex_timer = re.search(r'[0-9]{2}:[0-9]{2}:[0-9]{2}$', data, re.M)
    if regex_time:
        time_almost = regex_timer.group()
        time = datetime.datetime.strptime(time_almost, '%H:%M:%S').time()
        return time
    else:
        return sys.stderr.write("BAD FORMAT OF TIME")


# you can flight from point A->B and back to A but you can't flight A->B->A->B
# in list you have source and destination [A, B, B, A, A, B] and I compare A's and B's and you need minimal 6 to circle

def fly_to_circle(lst):
    if len(lst) >= 6:
        for i in range(len(lst)-6+1):
            if lst[i] == lst[i+3] and lst[i] == lst[i+4] and lst[i+3] == lst[i+4]:
                if lst[i+1] == lst[i+2] and lst[i+1] == lst[i+5] and lst[i+2] == lst[i+5]:
                    return True
                else:
                    return False
            else:
                return False
    else:
        return False


def del_first_and_last_line(s):
    s = s.split('\n', 1)[-1]
    if s.find('\n') == -1:
        return ''
    return s.rsplit('\n', 1)[0]


# DESCRIPTION:
# so i start this program in this format: cat input.csv | python3 python_task.py
# I find all combinations of flight split by new line and this combinations are separated by another new line
# I hope the format is right in input, I don't solve really long lines and problems with input file (i need learn how)
input_string = ""
for line in sys.stdin:
    input_string += str(line)

my_data = [x.split(',') for x in input_string.split('\n')]
del my_data[-1]  # because every file ends with \n i need cut off
length_of_list = len(my_data)
first_flight(my_data, length_of_list)